. libexec/xplatform_with_mac_dir.bash

# Cross-platform launcher for GNU patch

_xpatch () {
  _xplatform_with_mac_dir \
    '/usr/local/opt/gpatch/bin' patch "$@"
}

export -f _xpatch
