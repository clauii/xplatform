. libexec/xplatform_gnu.bash

# mcal: Enforce arguments for cal
_mcal () {
  local month
  local year

  month="${1?Usage: ${FUNCNAME[0]} MM YYYY}"
  year="${2?Usage: ${FUNCNAME[0]} MM YYYY}"

  _xplatform_gnu "${FUNCNAME[0]:2}" "$@"
}

export -f _mcal
