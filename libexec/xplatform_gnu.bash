. libexec/platform_id.bash

_xplatform_gnu () {
  local platform_id utility

  platform_id="$(_platform_id)" || return 1
  utility="${1?}"
  shift

  case "${platform_id}" in
    'MACOS')
      "/usr/local/bin/g${utility}" "$@"
      ;;
    'LINUX' | 'WIN')
      "${utility}" "$@"
      ;;
    *)
      return 1
      ;;
  esac
}

export -f _xplatform_gnu
