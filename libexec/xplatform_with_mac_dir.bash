. libexec/platform_id.bash

_xplatform_with_mac_dir () {
  local mac_dir platform_id utility

  platform_id="$(_platform_id)" || return 1

  mac_dir="${1?}"
  shift

  utility="${1?}"
  shift

  case "${platform_id}" in
    'MACOS')
      "${mac_dir}/${utility}" "$@"
      ;;
    'LINUX' | 'WIN')
      "${utility}" "$@"
      ;;
    *)
      return 1
      ;;
  esac
}

export -f _xplatform_with_mac_dir
