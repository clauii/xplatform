. libexec/xplatform_with_mac_dir.bash

# Cross-platform launcher for GNU getopt

_xgetopt () {
  _xplatform_with_mac_dir \
    '/usr/local/opt/gnu-getopt/bin' getopt "$@"
}

export -f _xgetopt
